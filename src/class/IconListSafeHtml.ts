import {SafeHtml} from '@angular/platform-browser';

export class IconListSafeHtml {
  title: string;
  iconVsg: SafeHtml;
  size: string;
}
