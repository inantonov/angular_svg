import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {IconListSafeHtml} from '../../class/IconListSafeHtml';

@Component({
  selector: 'app-icons-block',
  templateUrl: './icons-block.component.html',
  styleUrls: ['./icons-block.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IconsBlockComponent implements OnInit {
  @Input() iconGroup: IconListSafeHtml[];
  @Input() colorScheme: string;
  isHidden = false;

  constructor() {
  }

  ngOnInit() {
  }

}
