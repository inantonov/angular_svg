import {Component, OnInit} from '@angular/core';
import {GetIconsService} from '../get-icons.service';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {IconListSafeHtml} from '../../class/IconListSafeHtml';

@Component({
  selector: 'app-icons-list',
  templateUrl: './icons-list.component.html',
  styleUrls: ['./icons-list.component.scss']
})
export class IconsListComponent implements OnInit {

  iconList: IconListSafeHtml[] = [];
  searchTerm: string;
  colorScheme: string;

  constructor(private getIconsService: GetIconsService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.getIconList();
  }

  getIconList(): void {
    this.getIconsService.getIcons()
      .subscribe(iconList => {
        for (const index of Object.keys(iconList)) {
          const size = this.getSize(iconList[index]);
          this.iconList.push({
              title: index,
              iconVsg: this.prepareSvg(iconList[index]),
              size
            }
          );
        }
      })
      .unsubscribe();
  }

  prepareSvg(svg: string): SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(svg);
  }

  getSize(svg: string): string {
    const matcher = svg.match(/width="([0-9]*)/);
    if (matcher !== null) {
      return matcher[1];
    }
    return 'Infinity';
  }

  setFilter($event) {
    this.searchTerm = $event;
  }

  setColorScheme($event) {
    this.colorScheme = $event;
  }
}
