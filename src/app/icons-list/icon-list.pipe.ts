import {Pipe, PipeTransform} from '@angular/core';
import {IconListSafeHtml} from '../../class/IconListSafeHtml';
import {isArray, isUndefined} from 'util';

@Pipe({
  name: 'iconListFilterByTitle'
})
export class IconListFilterByTitlePipe implements PipeTransform {
  transform(iconList: IconListSafeHtml[], searchTerm: string): IconListSafeHtml[] {
    if (!iconList || !searchTerm) {
      return iconList;
    }
    return iconList.filter(icon => icon.title.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
  }
}

@Pipe({
  name: 'iconListFilterBySize'
})
export class IconListFilterBySizePipe implements PipeTransform {
  transform(iconList: IconListSafeHtml[], searchTerm: string): IconListSafeHtml[] {
    if (!iconList || !searchTerm) {
      return iconList;
    }
    return iconList.filter(icon => icon.size.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
  }
}

@Pipe({
  name: 'groupBy'
})
export class GroupByPipe implements PipeTransform {

  transform(input: any, prop: string): Array<any> {

    if (!isArray(input)) {
      return input;
    }

    const arr: { [key: string]: Array<any> } = {};

    for (const value of input) {
      const field: any = value.hasOwnProperty(prop) ? value[prop] : undefined;

      if (isUndefined(arr[field])) {
        arr[field] = [];
      }

      arr[field].push(value);
    }

    return Object.keys(arr).map(key => ({key, value: arr[key]}));
  }
}
