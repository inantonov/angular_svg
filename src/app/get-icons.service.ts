import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import {BASIC_ICONS} from '../mocks/basic_icons';
import {IconList} from '../class/IconList';




@Injectable({
  providedIn: 'root'
})
export class GetIconsService {

  constructor() { }

  getIcons(): Observable<IconList> {
    return of (BASIC_ICONS);
  }



}
