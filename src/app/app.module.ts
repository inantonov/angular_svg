import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsListComponent } from './icons-list/icons-list.component';
import { IconsBlockComponent } from './icons-block/icons-block.component';
import {GroupByPipe, IconListFilterByTitlePipe} from './icons-list/icon-list.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SearchInputComponent } from './search-input/search-input.component';
import { ColorSchemeComponent } from './color-scheme/color-scheme.component';

@NgModule({
  declarations: [
    AppComponent,
    IconsListComponent,
    IconsBlockComponent,
    GroupByPipe,
    IconListFilterByTitlePipe,
    SearchInputComponent,
    ColorSchemeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
