import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-color-scheme',
  templateUrl: './color-scheme.component.html',
  styleUrls: ['./color-scheme.component.scss']
})
export class ColorSchemeComponent implements OnInit, OnDestroy {
  value = 'default';
  colorSchemeControl = new FormControl('default');
  formCtrlSub: Subscription;
  @Output() onChangeColorScheme = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.onChangeColorScheme.emit('default');
    this.formCtrlSub = this.colorSchemeControl.valueChanges
      .subscribe(newValue => {
        this.value = newValue;
        this.onChangeColorScheme.emit(newValue);
      });
  }

  ngOnDestroy() {
    this.formCtrlSub.unsubscribe();
  }

}
