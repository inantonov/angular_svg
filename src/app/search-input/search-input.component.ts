import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit, OnDestroy {

  searchTemp = '';
  searchTempControl = new FormControl('');
  formCtrlSub: Subscription;
  @Output() onChangeInputValue = new EventEmitter();

  ngOnInit() {
    this.formCtrlSub = this.searchTempControl.valueChanges
      .pipe(
        debounceTime(300)
      )
      .subscribe(newValue => this.onChangeInputValue.emit(newValue));
  }

  ngOnDestroy() {
    this.formCtrlSub.unsubscribe();
  }
}
